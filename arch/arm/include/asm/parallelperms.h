/*
 * arm/arm/include/asm/parallelperms.h
 *
 * Copyright (C) 2017 Ali Razeen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  This file contains declarations to the parallel permissions implementation.
 */
#ifndef __ASMARM_PARALLELPERMS_H
#define __ASMARM_PARALLELPERMS_H

#define PP_ERR_OUTOFMEM				1
#define PP_ERR_INVALID_REQUESTED_MODE		2
#define PP_ERR_INVALID_ADDRESS			3
#define PP_ERR_ALREADY_MAPPED			4

#define PP_SUCCESS_NO_CHANGE			0
#define PP_SUCCESS_MMAP_B			1
#define PP_SUCCESS_MUNMAP_B			2

#define PP_SWITCH_TO_AS_A			1
#define PP_SWITCH_TO_AS_B			2

/* Use only the lower 24 bits to represent the different combinations of page
 * permissions. This is a hard requirement.
 */

#define PP_PERM_MIRROR_A	0 /* Use the same page permissions as in address
				   * space A. This must be zero.
				   */

/* Permissions defined by the ARMv7 spec. See ARM DDI 0406B, page B3-28. */
#define AP0		(1 << 4)
#define AP1		(2 << 4)
#define AP2		(1 << 9)

#define AP_000		(0)
#define AP_001		(AP0)
#define AP_010		(AP1)
#define AP_011		(AP1 + AP0)
#define AP_100		(AP2)
#define AP_101		(AP2 + AP0)
#define AP_110		(AP2 + AP1)
#define AP_111		(AP2 + AP1 + AP0)

#define PP_PERM_KERNEL_NONE_USER_NONE		(AP_000)
#define PP_PERM_KERNEL_RW_USER_NONE		(AP_001)
#define PP_PERM_KERNEL_RW_USER_R		(AP_010)
#define PP_PERM_KERNEL_RW_USER_RW		(AP_011)
#define PP_PERM_RESERVED_b100			(AP_100)
#define PP_PERM_KERNEL_R_USER_NONE		(AP_101)
#define PP_PERM_DEPRECATED_b110			(AP_110)
#define PP_PERM_KERNEL_R_USER_R			(AP_111)

#define PP_PERM_SEPARATE_PAGE			0x800000
#define PP_AP_BITS(perm_val)			((pp_perm_val) & AP_111);

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
#ifndef __ASSEMBLY__

// The permissions to apply in the second page table are stored in a different
// table-based data structure. This is the "permissions table" and it resembles
// a two-level page table. The top level permissions table has 1024 entries and
// each entry points to a second level table. A second level table also has 1024
// entry and each entry contains the permissions to use. The upper 10 bits of a
// virtual address are used to index into the top level table and the next 10
// bits of the address are used to index into the second level table.
#define PP_PERMS_PER_TOP_LEVEL			1024
#define PP_PERMS_TOP_LEVEL_MASK			0xFFC00000
#define PP_PERMS_SECOND_LEVEL_MASK		0x003FF000

#define PP_PERMS_TOP_LEVEL_SHIFT		22
#define PP_PERMS_SECOND_LEVEL_SHIFT		12

#define PP_PERMS_TOP_INDEX(addr)	(((addr) & PP_PERMS_TOP_LEVEL_MASK) >> PP_PERMS_TOP_LEVEL_SHIFT)
#define PP_PERMS_SECOND_INDEX(addr)	(((addr) & PP_PERMS_SECOND_LEVEL_MASK) >> PP_PERMS_SECOND_LEVEL_SHIFT)

#define PP_PERMS_TOP_SIZE	PAGE_SIZE
#define PP_PERMS_SECOND_SIZE	PAGE_SIZE

#define pp_perms_top_alloc()			(unsigned long **)__get_free_pages(GFP_KERNEL, 0)
#define pp_perms_top_free(perms)		free_pages((unsigned long)perms, 0);

#define pp_perms_second_alloc()			(unsigned long *)__get_free_pages(GFP_KERNEL, 0);
#define pp_perms_second_free(second_perms)	free_pages((unsigned long)second_perms, 0);

extern int pp_switch_perms(unsigned long mode);
extern int pp_mprotect_b(unsigned long addr, unsigned long length, unsigned long prot);
extern int pp_mmap_b(unsigned long addr, unsigned long length);
extern int pp_munmap_b(unsigned long addr, unsigned long length);

struct mm_struct;
extern void pp_dup_mmap(struct mm_struct *oldmm, struct mm_struct *mm);

#endif /* !__ASSEMBLY__ */
#endif /* CONFIG_PGTABLES_PARALLEL_PERMISSIONS */

#endif
