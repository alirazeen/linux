#include <asm/parallelperms.h>

#include <asm/mmu_context.h>
#include <asm/pgalloc.h>

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
int pp_switch_perms(unsigned long mode)
{
	struct thread_info *thread = current_thread_info();
	struct task_struct *tsk = thread->task;
	struct mm_struct *mm = tsk->mm;

	down_write(&mm->mmap_sem);

	if (unlikely(!mm->pgd_b))
		if (!pgd_alloc_b(mm))
			goto out_of_mem;

	if (mode == PP_SWITCH_TO_AS_A) {
		if (tsk->active_pgd == mm->pgd_a)
			goto no_change;

		tsk->active_pgd = mm->pgd_a;
		goto switched;

	} else if (mode == PP_SWITCH_TO_AS_B) {
		if (tsk->active_pgd == mm->pgd_b)
			goto no_change;

		tsk->active_pgd = mm->pgd_b;
		goto switched;
	}

	if (mode == PP_SWITCH_TO_AS_A) {
		if (tsk->active_pgd == mm->pgd_a)
			goto no_change;

		tsk->active_pgd = mm->pgd_a;
		goto switched;

	} else if (mode == PP_SWITCH_TO_AS_B) {
		if (tsk->active_pgd == mm->pgd_b)
			goto no_change;

		tsk->active_pgd = mm->pgd_b;
		goto switched;
	}

	up_write(&mm->mmap_sem);
	return -PP_ERR_INVALID_REQUESTED_MODE;

no_change:
	up_write(&mm->mmap_sem);
	return PP_SUCCESS_NO_CHANGE;

out_of_mem:
	up_write(&mm->mmap_sem);
	return -PP_ERR_OUTOFMEM;

switched:
	tsk->pgd_switched = true;
	switch_mm(tsk->mm, tsk->mm, tsk);
	up_write(&mm->mmap_sem);
	return mode;
}

/**
 * Must hold mm->mmap_sem semaphore when coming here.
 */
int pp_prepare_AS_b(struct mm_struct *mm, unsigned long start_addr, unsigned long end_addr)
{
	unsigned int index;
	pmd_t *pmd_a, *pmd_b;
	pte_t *pte_a, *pte_b;

	bool mapped_in_a = true;

	pte_t *start_pte;
	spinlock_t *ptl;
	unsigned long addr;
	pgtable_t new_pgtable = NULL;
	unsigned long *new_second_level_perms = NULL;

	for (addr = start_addr & PAGE_MASK; addr < end_addr; addr += (1 << PGDIR_SHIFT)) {
		/* Step 1, check if there is need to allocate a second-level page table
		 * for AS B. */
		index = pgd_index(addr);
		pmd_a = (pmd_t*)(mm->pgd_a + index);
		pmd_b = (pmd_t*)(mm->pgd_b + index);

		if (pmd_none(*pmd_a))
			mapped_in_a = false;
		else
			mapped_in_a = true;

		/**
		 * The second-level table for AS B has to be allocated if:
		 * - it hasn't been allocated yet, or ...
		 * - both AS A and AS B use the same second-level table.
		 */
		if (!pmd_none(*pmd_b)) {
			if (!pmd_none(*pmd_a)) {
				pte_a = pte_offset_map(pmd_a, addr);
				pte_b = pte_offset_map(pmd_b, addr);
				if (pte_a == pte_b)
					goto allocate_pages;
			}
			continue;
		}

allocate_pages:
		/* Step 2, allocate the the second-level table for AS B, and the
		 * space for the permission bits. */
		new_pgtable = pte_alloc_one(mm, addr);
		if (!new_pgtable)
			goto out_of_mem;;

		if (mapped_in_a) {
			start_pte = pte_offset_map_lock(mm, pmd_a, addr, &ptl);
			/* Copy everything from the second level of table A to
			 * the newly allocated page table. */
			memcpy((void*)page_to_virt(new_pgtable), __pte_map(pmd_a), PTE_TABLE_SIZE);
			pte_unmap_unlock(start_pte, ptl);
		} else {
			memset((void*)page_to_virt(new_pgtable), 0, PTE_TABLE_SIZE);
		}
		smp_wmb();

		pmd_populate_nosync(mm, pmd_b, new_pgtable);
	}

	/**
	 * Step 3, check if there is a need to allocate space to hold the
	 * permission bits.
	 */
	start_addr &= PP_PERMS_TOP_LEVEL_MASK;
	end_addr &= PP_PERMS_TOP_LEVEL_MASK;

	for (addr = start_addr; addr <= end_addr; addr += (1 << PP_PERMS_TOP_LEVEL_SHIFT)) {
		index = PP_PERMS_TOP_INDEX(addr);
		if (mm->pp_perms[index])
			continue;

		new_second_level_perms = pp_perms_second_alloc();
		if (!new_second_level_perms)
			goto out_of_mem;

		memset(new_second_level_perms, 0, PP_PERMS_SECOND_SIZE);
		mm->pp_perms[index] = new_second_level_perms;
	}

	return 1;

out_of_mem:
	return 0;
}

int allocate_page_in_b(struct mm_struct *mm, unsigned long addr, pte_t *pte_b, unsigned long pp_perm_val)
{
	pte_t new_pteval;
	struct page *new_page = NULL;

	new_page = alloc_page(GFP_HIGHUSER_MOVABLE);
	if (!new_page)
		goto out_of_mem;

	/* Zero out the allocated page, flush the dcache, and assign the virtual
	 * page mapping write permissions. */
	clear_user_highpage(new_page, addr);
	flush_dcache_page(new_page);
	new_pteval = mk_pte(new_page, __P011);
	pte_mkwrite(new_pteval);

	/* TODO: Perform some accounting somewhere to keep track of the memory
	 * used by this extra mapping? */
	set_pte_at_nosync(mm, addr, pte_b, new_pteval, pp_perm_val);

	/* Note: flush_tlb_range takes in a struct vma* as the first
	 * argument. Typically, we want this vma to be retrieved from
	 * find_extend_vma. However, since flush_tlb_range for ARMv7 only
	 * accesses vma->vm_mm, let's just pass in mm->mmap. This avoids the
	 * need to deal with the issue when we're allocating a page whose
	 * address is not mapped in the processes's VMA. */
	flush_tlb_range(mm->mmap, addr, addr + PAGE_SIZE);

	return 1;

out_of_mem:
	return 0;
}

int pp_mprotect_b(unsigned long addr, unsigned long length, unsigned long prot)
{
	unsigned int index;
	pmd_t *pmd_b;
	pte_t *pte_b;
	unsigned long *pte_b_perms;
	unsigned long start_addr, end_addr;

	struct vm_area_struct *vma = NULL;

	struct thread_info *thread = current_thread_info();
	struct task_struct *task = thread->task;
	struct mm_struct *mm = task->mm;

	down_write(&mm->mmap_sem);

	if (unlikely(!mm->pgd_b))
		if (!pgd_alloc_b(mm))
			goto out_of_mem;

	length = PAGE_ALIGN(length);
	if (length == 0)
		goto invalid_addr;

	addr &= PAGE_MASK;
	start_addr = addr;
	end_addr = (addr + length);

	if (addr >= TASK_SIZE || end_addr > TASK_SIZE)
		goto invalid_addr;

	for (addr = start_addr; addr < end_addr; addr += PAGE_SIZE) {
		vma = find_extend_vma(mm, addr);
		if (!vma || addr < vma->vm_start) {
			/* The requested address has not been mapped in address space
			 * A. If it isn't mapped in address space B either, then return
			 * an invalid address error. */
			index = PP_PERMS_TOP_INDEX(addr);
			pte_b_perms = mm->pp_perms[index];
			index = PP_PERMS_SECOND_INDEX(addr);
			if (!pte_b_perms || !(pte_b_perms[index] & PP_PERM_SEPARATE_PAGE))
				goto invalid_addr;
		}
	}

	if (unlikely(!pp_prepare_AS_b(mm, start_addr, end_addr)))
		goto out_of_mem;

	for (addr = start_addr; addr < end_addr; addr += PAGE_SIZE) {
		index = pgd_index(addr);
		pmd_b = (pmd_t*)(mm->pgd_b + index);
		pte_b = pte_offset_map(pmd_b, addr);

		index = PP_PERMS_TOP_INDEX(addr);
		pte_b_perms = mm->pp_perms[index];
		index = PP_PERMS_SECOND_INDEX(addr);
		pte_b_perms[index] = prot;

		set_pte_at_nosync(mm, addr, pte_b, pte_val(*pte_b), prot);
	}

	flush_tlb_range(mm->mmap, start_addr, end_addr);
	goto success;

out_of_mem:
	up_write(&mm->mmap_sem);
	return -PP_ERR_OUTOFMEM;

invalid_addr:
	up_write(&mm->mmap_sem);
	return -PP_ERR_INVALID_ADDRESS;

success:
	up_write(&mm->mmap_sem);
	return prot;
}

int pp_mmap_b(unsigned long addr, unsigned long length)
{
	unsigned int index;
	pmd_t *pmd_b;
	pte_t *pte_b;

	unsigned long *pte_b_perms;
	unsigned long pp_perm_val;
	unsigned long start_addr, end_addr;

	struct thread_info *thread = current_thread_info();
	struct task_struct *task = thread->task;
	struct mm_struct *mm = task->mm;

	down_write(&mm->mmap_sem);

	if (unlikely(!mm->pgd_b))
		if (!pgd_alloc_b(mm))
			goto out_of_mem;

	length = PAGE_ALIGN(length);
	if (length == 0)
		goto invalid_addr;

	addr &= PAGE_MASK;
	start_addr = addr;
	end_addr = (addr + length);

	if (addr >= TASK_SIZE || end_addr > TASK_SIZE)
		goto invalid_addr;

	for (addr = start_addr; addr < end_addr; addr += PAGE_SIZE) {
		if (unlikely(!pp_prepare_AS_b(mm, addr, addr + PAGE_SIZE)))
			goto out_of_mem;

		/* Check if the page in page table B has already been allocated. */
		index = PP_PERMS_TOP_INDEX(addr);
		pte_b_perms = mm->pp_perms[index];
		index = PP_PERMS_SECOND_INDEX(addr);
		pp_perm_val = pte_b_perms[index];

		if (pp_perm_val & PP_PERM_SEPARATE_PAGE)
			goto already_mapped;

		index = pgd_index(addr);
		pmd_b = (pmd_t*)(mm->pgd_b + index);
		pte_b = pte_offset_map(pmd_b, addr);

		pp_perm_val = (PP_PERM_SEPARATE_PAGE | PP_PERM_KERNEL_RW_USER_RW);
		index = PP_PERMS_TOP_INDEX(addr);
		pte_b_perms = mm->pp_perms[index];
		index = PP_PERMS_SECOND_INDEX(addr);
		pte_b_perms[index] = pp_perm_val;

		if (!allocate_page_in_b(mm, addr, pte_b, pp_perm_val))
			goto out_of_mem;
	}

	goto success;

out_of_mem:
	up_write(&mm->mmap_sem);
	return -PP_ERR_OUTOFMEM;

invalid_addr:
	up_write(&mm->mmap_sem);
	return -PP_ERR_INVALID_ADDRESS;

already_mapped:
	up_write(&mm->mmap_sem);
	return -PP_ERR_ALREADY_MAPPED;

success:
	up_write(&mm->mmap_sem);
	return PP_SUCCESS_MMAP_B;
}

int pp_munmap_b(unsigned long addr, unsigned long length)
{
	unsigned int index;
	pmd_t *pmd_a, *pmd_b;
	pte_t *pte_a, *pte_b;

	unsigned long *pte_b_perms;
	unsigned long pp_perm_val;
	unsigned long start_addr, end_addr;

	struct page *page_b = NULL;
	pte_t new_pteval = __pte(0);

	struct thread_info *thread = current_thread_info();
	struct task_struct *task = thread->task;
	struct mm_struct *mm = task->mm;

	down_write(&mm->mmap_sem);

	if (addr >= TASK_SIZE)
		goto invalid_addr;

	if (unlikely(!mm->pgd_b))
		goto no_change;

	length = PAGE_ALIGN(length);
	if (length == 0)
		goto invalid_addr;

	addr &= PAGE_MASK;
	start_addr = addr;
	end_addr = (addr + length);

	if (addr >= TASK_SIZE || end_addr > TASK_SIZE)
		goto invalid_addr;

	for (addr = start_addr; addr < end_addr; addr += PAGE_SIZE) {
		index = PP_PERMS_TOP_INDEX(addr);
		pte_b_perms = mm->pp_perms[index];
		if (!pte_b_perms)
			continue;

		index = PP_PERMS_SECOND_INDEX(addr);
		pp_perm_val = pte_b_perms[index];

		if (!(pp_perm_val & PP_PERM_SEPARATE_PAGE))
			continue;

		index = pgd_index(addr);
		pmd_a = (pmd_t*)(mm->pgd_a + index);

		if (unlikely(pmd_none(*pmd_a)))
			pte_a = NULL;
		else
			pte_a = pte_offset_map(pmd_a, addr);

		pmd_b = (pmd_t*)(mm->pgd_b + index);
		pte_b = pte_offset_map(pmd_b, addr);
		page_b = pte_page(pte_val(*pte_b));

		/* TODO: Perform the accounting as described in pp_mmap_b? */
		__free_page(page_b);

		pp_perm_val = PP_PERM_MIRROR_A;
		index = PP_PERMS_TOP_INDEX(addr);
		pte_b_perms = mm->pp_perms[index];
		index = PP_PERMS_SECOND_INDEX(addr);
		pte_b_perms[index] = pp_perm_val;

		if (pte_a)
			new_pteval = pte_val(*pte_a);

		set_pte_at_nosync(mm, addr, pte_b, new_pteval, pp_perm_val);
		flush_tlb_range(mm->mmap, addr, addr + PAGE_SIZE);
	}

	goto success;

invalid_addr:
	up_write(&mm->mmap_sem);
	return -PP_ERR_INVALID_ADDRESS;

no_change:
	up_write(&mm->mmap_sem);
	return PP_SUCCESS_NO_CHANGE;

success:
	up_write(&mm->mmap_sem);
	return PP_SUCCESS_MUNMAP_B;
}

void pp_dup_mmap(struct mm_struct *oldmm, struct mm_struct *mm)
{
	pmd_t *oldpmd_a, *oldpmd_b;
	pmd_t *newpmd_a, *newpmd_b;
	pte_t *oldpte_a, *oldpte_b, *newpte_b;

	int i, j;
	unsigned int index;
	unsigned long addr, pp_perm_val;
	unsigned long *oldperms;
	unsigned long *new_second_level_perms = NULL;

	pgtable_t new_pgtable = NULL;

	if (oldmm->pgd_b == NULL)
		return;

	/* First allocate the main pgd_b for the new mm. */
	pgd_alloc_b(mm);

	for (i = 0; i < USER_PTRS_PER_PGD; i++) {
		addr = (i << PGDIR_SHIFT);
		oldpmd_a = (pmd_t*)(oldmm->pgd_a + i);
		oldpmd_b = (pmd_t*)(oldmm->pgd_b + i);
		oldpte_a = __pte_map(oldpmd_a);
		oldpte_b = __pte_map(oldpmd_b);

		if (oldpte_a == oldpte_b)
			continue;

		new_pgtable = pte_alloc_one(mm, addr);
		if (!new_pgtable)
			goto out_of_mem;;

		/* Next, create the second level page tables for the new mm's
		 * pgd_b. */
		newpmd_a = (pmd_t*)(mm->pgd_a + i);
		newpmd_b = (pmd_t*)(mm->pgd_b + i);
		memcpy((void*)page_to_virt(new_pgtable), __pte_map(newpmd_a), PTE_TABLE_SIZE);
		smp_wmb();

		pmd_populate_nosync(mm, newpmd_b, new_pgtable);

		index = PP_PERMS_TOP_INDEX(addr);
		oldperms = oldmm->pp_perms[index];
		if (!oldperms)
			continue;

		new_second_level_perms = pp_perms_second_alloc();
		if (!new_second_level_perms)
			goto out_of_mem;

		memcpy(new_second_level_perms, oldperms, PP_PERMS_SECOND_SIZE);
		mm->pp_perms[index] = new_second_level_perms;

		/* The parent process might have created pages via calls to
		 * pp_mmap_b(...) for some virtual addresses. We need to do the
		 * same for the child process. */
		for (j = 0; j < PTRS_PER_PTE; j++, addr += (1 << PAGE_SHIFT)) {
			index = PP_PERMS_SECOND_INDEX(addr);
			pp_perm_val = oldperms[index];

			if (!(pp_perm_val & PP_PERM_SEPARATE_PAGE))
				continue;

			newpte_b = pte_offset_map(newpmd_b, addr);
			if (!allocate_page_in_b(mm, addr, newpte_b, pp_perm_val))
				goto out_of_mem;
		}
	}

	return;

out_of_mem:
	panic("Out of memory in pp_dup_mmap.");
}

#endif
