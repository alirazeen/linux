/*
 *  linux/arch/arm/mm/pgd.c
 *
 *  Copyright (C) 1998-2005 Russell King
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <linux/mm.h>
#include <linux/gfp.h>
#include <linux/highmem.h>
#include <linux/slab.h>

#include <asm/cp15.h>
#include <asm/pgalloc.h>
#include <asm/page.h>
#include <asm/tlbflush.h>

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
#include <asm/parallelperms.h>
#endif

#include "mm.h"

#ifdef CONFIG_ARM_LPAE
#define __pgd_alloc()	kmalloc(PTRS_PER_PGD * sizeof(pgd_t), GFP_KERNEL)
#define __pgd_free(pgd)	kfree(pgd)
#else
#define __pgd_alloc()	(pgd_t *)__get_free_pages(GFP_KERNEL, 2)
#define __pgd_free(pgd)	free_pages((unsigned long)pgd, 2)
#endif

/*
 * need to get a 16k page for level 1
 */
pgd_t *pgd_alloc(struct mm_struct *mm)
{
	pgd_t *new_pgd, *init_pgd;
	pud_t *new_pud, *init_pud;
	pmd_t *new_pmd, *init_pmd;
	pte_t *new_pte, *init_pte;

	new_pgd = __pgd_alloc();
	if (!new_pgd)
		goto no_pgd;

	memset(new_pgd, 0, USER_PTRS_PER_PGD * sizeof(pgd_t));

	/*
	 * Copy over the kernel and IO PGD entries
	 */
	init_pgd = pgd_offset_k(0);
	memcpy(new_pgd + USER_PTRS_PER_PGD, init_pgd + USER_PTRS_PER_PGD,
		       (PTRS_PER_PGD - USER_PTRS_PER_PGD) * sizeof(pgd_t));

	clean_dcache_area(new_pgd, PTRS_PER_PGD * sizeof(pgd_t));

#ifdef CONFIG_ARM_LPAE
	/*
	 * Allocate PMD table for modules and pkmap mappings.
	 */
	new_pud = pud_alloc(mm, new_pgd + pgd_index(MODULES_VADDR),
			    MODULES_VADDR);
	if (!new_pud)
		goto no_pud;

	new_pmd = pmd_alloc(mm, new_pud, 0);
	if (!new_pmd)
		goto no_pmd;
#endif

	if (!vectors_high()) {
		/*
		 * On ARM, first page must always be allocated since it
		 * contains the machine vectors. The vectors are always high
		 * with LPAE.
		 */
		new_pud = pud_alloc(mm, new_pgd, 0);
		if (!new_pud)
			goto no_pud;

		new_pmd = pmd_alloc(mm, new_pud, 0);
		if (!new_pmd)
			goto no_pmd;

		new_pte = pte_alloc_map(mm, new_pmd, 0);
		if (!new_pte)
			goto no_pte;

#ifndef CONFIG_ARM_LPAE
		/*
		 * Modify the PTE pointer to have the correct domain.  This
		 * needs to be the vectors domain to avoid the low vectors
		 * being unmapped.
		 */
		pmd_val(*new_pmd) &= ~PMD_DOMAIN_MASK;
		pmd_val(*new_pmd) |= PMD_DOMAIN(DOMAIN_VECTORS);
#endif

		init_pud = pud_offset(init_pgd, 0);
		init_pmd = pmd_offset(init_pud, 0);
		init_pte = pte_offset_map(init_pmd, 0);
		set_pte_ext(new_pte + 0, init_pte[0], 0);
		set_pte_ext(new_pte + 1, init_pte[1], 0);
		pte_unmap(init_pte);
		pte_unmap(new_pte);
	}

	return new_pgd;

no_pte:
	pmd_free(mm, new_pmd);
	mm_dec_nr_pmds(mm);
no_pmd:
	pud_free(mm, new_pud);
no_pud:
	__pgd_free(new_pgd);
no_pgd:
	return NULL;
}

void pgd_free(struct mm_struct *mm, pgd_t *pgd_base)
{
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pgtable_t pte;

	if (!pgd_base)
		return;

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
	pgd_free_b(mm);
#endif

	pgd = pgd_base + pgd_index(0);
	if (pgd_none_or_clear_bad(pgd))
		goto no_pgd;

	pud = pud_offset(pgd, 0);
	if (pud_none_or_clear_bad(pud))
		goto no_pud;

	pmd = pmd_offset(pud, 0);
	if (pmd_none_or_clear_bad(pmd))
		goto no_pmd;

	pte = pmd_pgtable(*pmd);
	pmd_clear(pmd);
	pte_free(mm, pte);
	atomic_long_dec(&mm->nr_ptes);
no_pmd:
	pud_clear(pud);
	pmd_free(mm, pmd);
	mm_dec_nr_pmds(mm);
no_pud:
	pgd_clear(pgd);
	pud_free(mm, pud);
no_pgd:
#ifdef CONFIG_ARM_LPAE
	/*
	 * Free modules/pkmap or identity pmd tables.
	 */
	for (pgd = pgd_base; pgd < pgd_base + PTRS_PER_PGD; pgd++) {
		if (pgd_none_or_clear_bad(pgd))
			continue;
		if (pgd_val(*pgd) & L_PGD_SWAPPER)
			continue;
		pud = pud_offset(pgd, 0);
		if (pud_none_or_clear_bad(pud))
			continue;
		pmd = pmd_offset(pud, 0);
		pud_clear(pud);
		pmd_free(mm, pmd);
		mm_dec_nr_pmds(mm);
		pgd_clear(pgd);
		pud_free(mm, pud);
	}
#endif
	__pgd_free(pgd_base);
}

#ifdef CONFIG_PGTABLES_PARALLEL_PERMISSIONS
pgd_t *pgd_alloc_b(struct mm_struct *mm)
{
	pgd_t *init_pgd, *new_pgd;
	unsigned long **new_pp_perms;

	new_pgd = __pgd_alloc();
	if (!new_pgd)
		goto no_pgd;

	new_pp_perms = pp_perms_top_alloc();
	if (!new_pp_perms) {
		__pgd_free(new_pgd);
		goto no_pgd;
	}

	/*
	 * Copy over the user-space mappings
	 */
	spin_lock(&mm->page_table_lock);
	if (likely(!mm->pgd_b)) {
		mm->pgd_b = new_pgd;
		mm->pp_perms = new_pp_perms;
		new_pgd = NULL;
		memcpy(mm->pgd_b, mm->pgd_a, USER_PTRS_PER_PGD * sizeof(pgd_t));
		memset(new_pp_perms, 0, PP_PERMS_TOP_SIZE);
	}
	spin_unlock(&mm->page_table_lock);

	if (new_pgd) {
		__pgd_free(new_pgd);
		pp_perms_top_free(new_pp_perms);
		goto success;
	}

	/*
	 * Copy over the kernel and IO PGD entries
	 */
	spin_lock(&init_mm.page_table_lock);
	init_pgd = pgd_offset_k(0);
	memcpy(mm->pgd_b + USER_PTRS_PER_PGD, init_pgd + USER_PTRS_PER_PGD,
	       (PTRS_PER_PGD - USER_PTRS_PER_PGD) * sizeof(pgd_t));
	spin_unlock(&init_mm.page_table_lock);

	clean_dcache_area(mm->pgd_b, PTRS_PER_PGD * sizeof(pgd_t));

success:
	return mm->pgd_b;

no_pgd:
	return NULL;
}

void pgd_free_b(struct mm_struct *mm)
{
	unsigned long *perms;
	pmd_t *pmd_a, *pmd_b;
	pte_t *pte_a, *pte_b;
	pte_t pte_b_val;
	struct page *page_b;

	unsigned long addr, pp_perm_val;
	unsigned int index;
	int i, j;

	if (!mm->pgd_b)
		goto skip_pgd_free;

	for (i = 0; i < USER_PTRS_PER_PGD; i++) {
		addr = (i << PGDIR_SHIFT);
		pmd_a = (pmd_t*)(mm->pgd_a + i);
		pmd_b = (pmd_t*)(mm->pgd_b + i);
		pte_a = __pte_map(pmd_a);
		pte_b = __pte_map(pmd_b);

		if (pte_a != pte_b) {

			/* Check if we have to free pages allocated by calls to
			 * pp_mmap_b(...). */
			for (j = 0; j < PTRS_PER_PTE; j++, addr += (1 << PAGE_SHIFT)) {
				index = PP_PERMS_TOP_INDEX(addr);
				perms = mm->pp_perms[index];
				if (!perms)
					continue;

				index = PP_PERMS_SECOND_INDEX(addr);
				pp_perm_val = perms[index];
				if (!(pp_perm_val & PP_PERM_SEPARATE_PAGE))
					continue;

				pte_b_val = pte_val(*(pte_b + j));
				page_b = pte_page(pte_b_val);
				__free_page(page_b);
			}

			pte_free(mm, virt_to_page((unsigned long)pte_b));
		}
	}

	__pgd_free(mm->pgd_b);

skip_pgd_free:

	if (!mm->pp_perms)
		goto skip_pp_perms_free;

	for (i = 0; i < PP_PERMS_PER_TOP_LEVEL; i++) {
		perms = mm->pp_perms[i];
		if (perms)
			pp_perms_second_free(perms);
	}

	pp_perms_top_free(mm->pp_perms);

skip_pp_perms_free:
	return;
}
#endif
